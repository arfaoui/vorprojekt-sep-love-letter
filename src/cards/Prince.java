package cards;

import game.Game;
import game.Player;

public class Prince extends Cards {
 String s ;
 Player chosenPlayer ;
 String chosenName;
 Boolean tr = true;


    public  Prince () {
        cardValue = 5;
        nameOfCard = "Prince";
        power = "Choose a player. They discard their hand and draw a new card.";
    }
    // asks the player to choose a player
    // if all active players of the round are protected the player should choose him self
    // output the names of the active players
    public void action (Game game, Player player){


        System.out.println("Choose a player to discard their hand and draw a new one.");
        System.out.println("------------------------------------------------------------------------");
        System.out.println(("you can choose between"));
        for (int i = 0; i< game.activePlayers.size(); i++){
            System.out.println(game.activePlayers.get(i).getName());
        }

        if (checkAll(game, player)) {
            System.out.println(player.name + " you are forced to choose your self");
            System.out.println("------------------------------------------------------------------------");
            playPrince(game,player,player);
        }else{
        System.out.println("enter name of the player you want to choose");
        boolean tr = true;
        while (tr) {
                s = scan.next();


                if ( checkIfTheNameExists(game , s)) {
                        chosenName = s;
                        tr = false;
                        chosenPlayer = game.getPlayerByUserName(chosenName);
                        System.out.println("you choosed " + chosenName);
                        playPrince(game, game.activePlayers.get(0), chosenPlayer);


                        System.out.println("------------------------------------------------------------------------");
                    }else {
                        System.out.println("the name you typed doesn't exist please type a name from the list");
                        System.out.println("------------------------------------------------------------------------");
                    }
      }
    }}
    // the chosen player discard his card draw another one
        public void playPrince (Game game, Player currentPlayer , Player chosenPlayer ){
        if (chosenPlayer.playerProtected()){
            System.out.println(chosenPlayer.getName() + " is protected you discard a card without effect");

        }else{
            game.noEffect (chosenPlayer.showHand().remove(0), chosenPlayer);
        }
            while (t) {
                String commands = scan.next();
                switch (commands) {

                    case "\\help":
                        game.Commands helps = new game.Commands();
                        helps.help();
                        System.out.println();
                        break;

                    case "\\playCard":
                        game.Commands playC = new game.Commands();
                        playC.playCard();
                        System.out.println(game.activePlayers.get(0).name);
                        break;

                    case "\\showHand":
                        game.Commands show = new game.Commands();
                        show.showHand(currentPlayer);

                        break;


                    case "\\showScore":
                        game.Commands showS = new game.Commands();
                        showS.showScore(game, currentPlayer);
                        break;
                    case "\\showActivePlayerInTheRound":
                        game.Commands showAP = new game.Commands();
                        showAP.showActivePlayerInTheRound(game, currentPlayer);

                        break;
                    case "\\showAllPlayers":
                        game.Commands showA = new game.Commands();
                        showA.showAllPlayers(game, currentPlayer);
                        break;
                    case "\\exit":
                        game.setGameIsRunning();

                    case "\\goNext":

                        game.activePlayers.remove(currentPlayer);
                        game.activePlayers.add(currentPlayer);
                        game.turnMove(game.activePlayers.get(0));
                        t = false;
                        break;

    }}}
    // check if all active players are protected or not
    public boolean checkAll (Game game , Player player) {
        for (int i = 1; i < game.getActivePlayers().size(); i++) {
            if (!player.name.equals( game.getActivePlayers().get(i).getName())) {
                if (!game.activePlayers.get(i).playerProtected()) {
                    return false;
                }
            }
        }
        return true;
    }
    // check if the name typed by the current player exist in the list of the active players or not

    public boolean checkIfTheNameExists (Game game , String name){
        boolean check = false;
        for (Player player : game.activePlayers) {
            if (name.equals(player.getName())){
                check = true;
            }
        }
        return check;
    }}
