package cards;

import game.Game;
import game.Player;

public class Countess extends Cards {


    public  Countess () {
        cardValue = 7;
        nameOfCard = "Countess";
        power = "Must be played if you have King or Prince in hand";
    }
    // out put the effect of the countess
    // condition of countess and  playing countess are implemented in the Player Class
    //

    public void action (Game game, Player player){
        System.out.println("Must be played if you have King or Prince in hand");
        System.out.println("------------------------------------------------------------------------");

        while (t) {
            String commands = scan.next();
            switch (commands) {

                case "\\help":
                    game.Commands helps = new game.Commands();
                    helps.help();
                    System.out.println();
                    break;

                case "\\playCard":
                    game.Commands playC = new game.Commands();
                    playC.playCard();
                    System.out.println(game.activePlayers.get(0).name);
                    break;

                case "\\showHand":
                    game.Commands show = new game.Commands();
                    show.showHand(player);

                    break;


                case "\\showScore":
                    game.Commands showS = new game.Commands();
                    showS.showScore(game, player);
                    break;
                case "\\showActivePlayerInTheRound":
                    game.Commands showAP = new game.Commands();
                    showAP.showActivePlayerInTheRound(game, player);

                    break;
                case "\\showAllPlayers":
                    game.Commands showA = new game.Commands();
                    showA.showAllPlayers(game, player);
                    break;
                case "\\exit":
                    game.setGameIsRunning();

                case "\\goNext":

                    game.activePlayers.remove(player);
                    game.activePlayers.add(player);
                    game.turnMove(game.activePlayers.get(0));
                    t = false;
                    break;

            }
        }

    }
}