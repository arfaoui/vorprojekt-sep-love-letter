package cards;

import game.Game;
import game.Player;

import java.awt.*;
import java.nio.file.FileSystemNotFoundException;

public class King extends Cards {
    String s ;
    Player chosenPlayer ;
    String chosenName;
    Boolean tr = true;



    public  King() {

        cardValue = 6;
        nameOfCard = "King";
        power = " Trade hands with any other player.";

    }
    // asks to choose a card
    // change hands between the current player and chosen one if he is not protected
    public void action (Game game, Player player ) {

        System.out.println("Trade hands with any other player.");
        System.out.println("you can choose between: ");
        for (int i = 1; i < game.getActivePlayers().size(); i++) {
            System.out.println(game.getActivePlayers().get(i).getName());
        }
        System.out.println(" enter name of the player you want to choose");
        System.out.println("------------------------------------------------------------------------");
        boolean t = true;

        while (t) {
            s = scan.next();

            if (checkIfTheNameExists (game ,  s)) {
                chosenName = s;
                chosenPlayer = game.getPlayerByUserName(chosenName);
                t = false;
                System.out.println("you choosed " + chosenPlayer.getName());
                System.out.println("------------------------------------------------------------------------");
                    playKing(game , game.activePlayers.get(0), chosenPlayer);
                    System.out.println("the name you typed doesn't exist please type a name from the list");
                }
                System.out.println("------------------------------------------------------------------------");
            }}
        public void playKing (Game game, Player currentPlayer, Player chosenPalyer ){
            if (chosenPlayer.playerProtected()) {
                System.out.println(chosenPalyer.getName() + " is protected");
                System.out.println(chosenPalyer.getName() + " you discarded the king without effect");
                currentPlayer.getCardsDiscarded().add(new King());
            }else{
                Cards currenPlayerCard = currentPlayer.showHand().remove(0);
                Cards chosenPlayerCard = chosenPalyer.showHand().remove(0);
                currentPlayer.showHand().add(chosenPlayerCard);
                chosenPalyer.showHand().add(chosenPlayerCard);
                System.out.println(currentPlayer.getName() + " and " + chosenPalyer.getName() + " changed hands ");
                System.out.println("------------------------------------------------------------------------");

            }
            while (t) {
            String commands = scan.next();
            switch (commands) {

                case "\\help":
                    game.Commands helps = new game.Commands();
                    helps.help();
                    System.out.println();
                    break;

                case "\\playCard":
                    game.Commands playC = new game.Commands();
                    playC.playCard();
                    System.out.println(game.activePlayers.get(0).name);
                    break;

                case "\\showHand":
                    game.Commands show = new game.Commands();
                    show.showHand(currentPlayer);

                    break;


                case "\\showScore":
                    game.Commands showS = new game.Commands();
                    showS.showScore(game, currentPlayer);
                    break;
                case "\\showActivePlayerInTheRound":
                    game.Commands showAP = new game.Commands();
                    showAP.showActivePlayerInTheRound(game, currentPlayer);

                    break;
                case "\\showAllPlayers":
                    game.Commands showA = new game.Commands();
                    showA.showAllPlayers(game, currentPlayer);
                    break;
                case "\\exit":
                  game.setGameIsRunning();

                case "\\goNext":

                    game.activePlayers.remove(currentPlayer);
                    game.activePlayers.add(chosenPalyer);
                    game.turnMove(game.activePlayers.get(0));
                    t = false;
                    break;
            }}}
    // check if the name typed by the current player exist in the list of the active players or not
    public boolean checkIfTheNameExists (Game game , String name){
        boolean check = false;
        for (Player player : game.activePlayers) {
            if (name.equals(player.getName())){
                check = true;
            }
        }
        return check;
    }}



