package cards;

import game.Game;
import game.Player;

import java.util.Scanner;


public abstract class Cards {

    protected int cardValue;
    protected String nameOfCard;
    protected String power;
    Scanner scan = new Scanner(System.in);
    boolean t = true;


    public int getCardValue() {
        return cardValue;
    }

    public String getNameOfCard() {
        return nameOfCard;
    }

    public String getPower() {
        return power;
    }
    public abstract void action (Game game, Player player);



}
