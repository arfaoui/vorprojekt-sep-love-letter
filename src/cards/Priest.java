package cards;

import game.Game;
import game.Player;

import javax.swing.text.StyledEditorKit;

public class Priest extends Cards {
    public String s;
    public String chosenName;
    public Player chosenPlayer;


    public Priest() {
        cardValue = 2;
        nameOfCard = "Priest";
        power = "Look at a player's hand";
    }
    // asks the player to choose a player
    // output the names of the active players

    public void action(Game game, Player player) {

        System.out.println("Look at a player's hand");
        System.out.println("choose a player");
        System.out.println("you can choose between:");
        for (int i = 1; i < game.getActivePlayers().size(); i++) {
            System.out.println(game.getActivePlayers().get(i).getName());

        }
        System.out.println(" enter name of the player you want to choose");
        System.out.println("------------------------------------------------------------------------");


         Boolean t = true;
        while (t) {
            s = scan.next();

                if (checkIfTheNameExists(game , s)) {
                    chosenName = s;
                    chosenPlayer = game.getPlayerByUserName(chosenName);
                    System.out.println("you choosed " + chosenName);
                    playPriest(game, game.activePlayers.get(0), chosenPlayer);
                    t = false;
                }else {
                    System.out.println("the name you typed doesn't exist please type a name from the list");
                }}
                System.out.println("------------------------------------------------------------------------");


        }

     // check if chosen player is protected or not
    // output the name of chosen player and his hand
    public void playPriest(Game game, Player currentPlayer, Player chosenPlayer) {
        if (chosenPlayer.playerProtected()) {
            System.out.println(chosenPlayer.getName() + " is protected");
            System.out.println(" you discarded the priest without an effect");

        } else {
            System.out.println(chosenPlayer.getName() + " has a " + chosenPlayer.showHand().get(0).getNameOfCard());

        }
        System.out.println("------------------------------------------------------------------------");
        while (t) {
            String commands = scan.next();
            switch (commands) {

                case "\\help":
                    game.Commands helps = new game.Commands();
                    helps.help();
                    System.out.println();
                    break;

                case "\\playCard":
                    game.Commands playC = new game.Commands();
                    playC.playCard();
                    System.out.println(game.activePlayers.get(0).name);
                    break;

                case "\\showHand":
                    game.Commands show = new game.Commands();
                    show.showHand(currentPlayer);

                    break;


                case "\\showScore":
                    game.Commands showS = new game.Commands();
                    showS.showScore(game, currentPlayer);
                    break;
                case "\\showActivePlayerInTheRound":
                    game.Commands showAP = new game.Commands();
                    showAP.showActivePlayerInTheRound(game, currentPlayer);

                    break;
                case "\\showAllPlayers":
                    game.Commands showA = new game.Commands();
                    showA.showAllPlayers(game, currentPlayer);
                    break;
                case "\\exit":
                    game.setGameIsRunning();

                case "\\goNext":

                    game.activePlayers.remove(currentPlayer);
                    game.activePlayers.add(currentPlayer);
                    game.turnMove(game.activePlayers.get(0));
                    t = false;
                    break;
            }}
    }
    // check if the name typed by the current player exist in the list of the active players or not
    public boolean checkIfTheNameExists (Game game , String name){
        boolean check = false;
        for (Player player : game.activePlayers) {
            if (name.equals(player.getName())){
                check = true;
            }
        }
        return check;
    }}