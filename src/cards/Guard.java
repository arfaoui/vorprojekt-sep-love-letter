package cards;

import game.Game;
import game.Player;

public class Guard extends Cards {

    private static Player chosenPlayer = null;
    public String chosenName = "";
    String s;
    String guessedCrad;

    public Guard() {
        cardValue = 1;
        nameOfCard = "Guard";
        power = "Guess a player's hand; " +
                " if correct the player is out";
    }
  // asks the player to choose a player
  // output the names of the active players
  // asks to guess a card
  // if the guess is right chosen player is out

    public void action(Game game, Player player) {
        System.out.println("Guess a player's hand; " +
                " if correct the player is out");
        System.out.println("you can choose between:");
        for (int i = 1; i < game.getActivePlayers().size(); i++) {
            System.out.println(game.getActivePlayers().get(i).getName());
        }
        System.out.println(" enter name of the player you want to choose");
        System.out.println("------------------------------------------------------------------------");


        boolean t = true;

        while (t) {
            s = scan.next();

                if (checkIfTheNameExists (game ,  s)) {
                    chosenName = s;
                    chosenPlayer = game.getPlayerByUserName(chosenName);
                    t = false;
                    System.out.println("you choosed " + chosenPlayer.getName());
                    System.out.println("------------------------------------------------------------------------");
                    if (chosenPlayer.playerProtected()){
                        playGuard( game, game.getActivePlayers().get(0), chosenPlayer );
                        System.out.println("------------------------------------------------------------------------");
                    }else {
                        boolean tr = true;
                        System.out.println("you can guess between : prince, king , countess,");
                        System.out.println("princess, baron, handmaid , priest ");
                        System.out.println("enter name of card");
                        while (tr) {
                            s = scan.next();
                            switch (s) {
                                case "\\prince":
                                    guessedCrad = "Prince";
                                    tr = false;
                                    break;
                                case "\\king":
                                    guessedCrad = "King";
                                    tr = false;
                                    break;
                                case "\\countess":
                                    guessedCrad = "Countess";
                                    tr = false;
                                    break;
                                case "\\princess":
                                    guessedCrad = "Princess";
                                    tr = false;
                                    break;
                                case "\\baron":
                                    guessedCrad = "Baron";
                                    tr = false;
                                    break;
                                case "\\priest":
                                    guessedCrad = "Priest";
                                    tr = false;
                                    break;
                                case "\\handmaid":
                                    guessedCrad = "Handmaid";
                                    tr = false;
                                    break;
                                default:
                                    System.out.println("you have to enter a card name ");
                            }}
                            guessByName(game, game.getActivePlayers().get(0), chosenPlayer, guessedCrad);
                            System.out.println("------------------------------------------------------------------------");
                        }


                } else {
                    System.out.println("the name you typed doesn't exist please type a name from the list");
                }
            System.out.println("------------------------------------------------------------------------");

                }

        }


   // to check if the player is protected or not
    public void playGuard(Game game, Player currentPlayer, Player chosenPlayer) {
        if (chosenPlayer.playerProtected()) {
            System.out.println("the chosen player is protected your card has no effect");


            while (t) {
                String commands = scan.next();
                switch (commands) {

                    case "\\help":
                        game.Commands helps = new game.Commands();
                        helps.help();
                        System.out.println();
                        break;

                    case "\\playCard":
                        game.Commands playC = new game.Commands();
                        playC.playCard();
                        System.out.println(game.activePlayers.get(0).name);
                        break;

                    case "\\showHand":
                        game.Commands show = new game.Commands();
                        show.showHand(currentPlayer);

                        break;


                    case "\\showScore":
                        game.Commands showS = new game.Commands();
                        showS.showScore(game, currentPlayer);
                        break;
                    case "\\showActivePlayerInTheRound":
                        game.Commands showAP = new game.Commands();
                        showAP.showActivePlayerInTheRound(game, currentPlayer);

                        break;
                    case "\\showAllPlayers":
                        game.Commands showA = new game.Commands();
                        showA.showAllPlayers(game, currentPlayer);
                        break;
                    case "\\exit":
                        game.setGameIsRunning();

                    case "\\goNext":

                        game.activePlayers.remove(currentPlayer);
                        game.activePlayers.add(currentPlayer);
                        game.turnMove(game.activePlayers.get(0));
                        t = false;
                        break;

                }

            }
        }
    }
 // a methode to check if the player guessed the right name or not
    public void guessByName(Game game, Player currentPlayer, Player chosenPlayer , String cardName) {



        if (cardName.equals(chosenPlayer.showHand().get(0).nameOfCard )) {
            System.out.println(currentPlayer.getName() + " guessed right ");
            System.out.println(chosenPlayer.getName() + " is out");
            game.discardedCards.add(chosenPlayer.showHand().remove(0));
            game.getActivePlayers().remove(chosenPlayer);
            game.getNextRoundActivePlayer().add(chosenPlayer);
        } else {
            System.out.println (cardName + ":" + " your guess is wrong ") ;


        }
        while (t) {
            String commands = scan.next();
            switch (commands) {

                case "\\help":
                    game.Commands helps = new game.Commands();
                    helps.help();
                    System.out.println();
                    break;

                case "\\playCard":
                    game.Commands playC = new game.Commands();
                    playC.playCard();
                    System.out.println(game.activePlayers.get(0).name);
                    break;

                case "\\showHand":
                    game.Commands show = new game.Commands();
                    show.showHand(currentPlayer);

                    break;


                case "\\showScore":
                    game.Commands showS = new game.Commands();
                    showS.showScore(game, currentPlayer);
                    break;
                case "\\showActivePlayerInTheRound":
                    game.Commands showAP = new game.Commands();
                    showAP.showActivePlayerInTheRound(game, currentPlayer);

                    break;
                case "\\showAllPlayers":
                    game.Commands showA = new game.Commands();
                    showA.showAllPlayers(game, currentPlayer);
                    break;
                case "\\exit":
                    game.setGameIsRunning();

                case "\\goNext":

                    game.activePlayers.remove(currentPlayer);
                    game.activePlayers.add(currentPlayer);
                    game.turnMove(game.activePlayers.get(0));
                    t = false;
                    break;

        }}
    }
    // check if the name typed by the current player exist in the list of the active players or not
    public boolean checkIfTheNameExists (Game game , String name){
        boolean check = false;
        for (Player player : game.activePlayers) {
            if (name.equals(player.getName())){
                check = true;
            }
        }
        return check;
    }}
