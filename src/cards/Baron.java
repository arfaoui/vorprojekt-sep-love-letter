package cards;

import game.Game;
import game.Player;

public class Baron extends Cards {
    private static Player chosenPlayer = null;
    public String chosenName = "";
    String s;
    String guessedCrad;



    public  Baron () {
        cardValue = 3;
        nameOfCard = "Baron";
        power = "Compare hands with another player, the one with the lower value is out.";
    }
    // asks the player to choose a player
    // output the names of the active players
    // if the current player's hand is higher than the chosen player's the chosen player is out and vice_versa
    public void action (Game game, Player currentPlayer){
        System.out.println("Compare hands with another player, the one with the lower value is out.");

        System.out.println("you can choose between:");
        for (int i = 1; i < game.getActivePlayers().size(); i++) {
            System.out.println(game.getActivePlayers().get(i).getName());
        }
        System.out.println(" enter name of the player you want to choose");
        System.out.println("------------------------------------------------------------------------");
        boolean tr = true;

        while (tr) {
            s = scan.next();
                if (checkIfTheNameExists(game, s )) {
                    chosenName = s;
                    chosenPlayer = game.getPlayerByUserName(chosenName);
                    tr = false;
                    System.out.println("you choosed " + chosenPlayer.getName());
                    playBaron(game, game.getActivePlayers().get(0), chosenPlayer);
                    System.out.println("------------------------------------------------------------------------");
                }else {
                    System.out.println("the name you typed doesn't exist please type a name from the list");

                }
        }
        System.out.println("------------------------------------------------------------------------");
    }

        public void playBaron (Game game,  Player currentPlayer, Player chosenPlayer){

        if (chosenPlayer.playerProtected()){
           System.out.println("The chosen player "+ chosenPlayer.getName() + "is protected ");

               }else if (currentPlayer.showHand().get(0).getCardValue() > chosenPlayer.showHand().get(0).
                              getCardValue()){
                          System.out.println("the chosen player " + chosenPlayer.getName()+ " has a lower number " );
                          System.out.println(chosenPlayer.getName() + " is out");
                          game.getActivePlayers().remove(chosenPlayer);
                          game.getNextRoundActivePlayer().add(0,chosenPlayer);


                          }else if (currentPlayer.showHand().get(0).getCardValue() < chosenPlayer.showHand().get(0).getCardValue()){
                          System.out.println("the chosen player " + chosenPlayer.getName()+ " has a higher number " );
                          System.out.println(currentPlayer.getName() + " is out");
                          game.getActivePlayers().remove(currentPlayer);
                          game.getNextRoundActivePlayer().add(0, currentPlayer);

                                  }else{

                                      while (t) {
                                          String commands = scan.next();
                                          switch (commands) {

                                              case "\\help":
                                                  game.Commands helps = new game.Commands();
                                                  helps.help();
                                                  System.out.println();
                                                  break;

                                              case "\\playCard":
                                                  game.Commands playC = new game.Commands();
                                                  playC.playCard();
                                                  System.out.println(game.activePlayers.get(0).name);
                                                  break;

                                              case "\\showHand":
                                                  game.Commands show = new game.Commands();
                                                  show.showHand(currentPlayer);

                                                  break;


                                              case "\\showScore":
                                                  game.Commands showS = new game.Commands();
                                                  showS.showScore(game, currentPlayer);
                                                  break;
                                              case "\\showActivePlayerInTheRound":
                                                  game.Commands showAP = new game.Commands();
                                                  showAP.showActivePlayerInTheRound(game, currentPlayer);

                                                  break;
                                              case "\\showAllPlayers":
                                                  game.Commands showA = new game.Commands();
                                                  showA.showAllPlayers(game, currentPlayer);
                                                  break;
                                              case "\\exit":
                                                  game.setGameIsRunning();

                                              case "\\goNext":

                                                  game.activePlayers.remove(currentPlayer);
                                                  game.activePlayers.add(currentPlayer);
                                                  game.turnMove(game.activePlayers.get(0));
                                                  t = false;
                                                  break;
                                          }
                                      }


            }
    }
    // check if the name typed by the current player exist in the list of the active players or not
    public boolean checkIfTheNameExists (Game game , String name){
        boolean check = false;
        for (Player player : game.activePlayers) {
            if (name.equals(player.getName())){
                check = true;
            }
        }
        return check;
    }}























