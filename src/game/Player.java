package game;

import cards.Cards;
import cards.Prince;
import cards.Princess;
import game.Game;

import java.util.ArrayList;
import java.util.List;

public class Player {
    // players score
    public int score = 0;
    // players name
    public String name;
    // players hand
    public ArrayList<Cards> hand = new ArrayList<Cards>(2);
    // players situation if is protected or not


    // players are not protected at the start of the game
    private boolean playerProtected = false;
    // list of cards that the player discarded
    ArrayList<Cards> cardsDiscarded = new ArrayList<Cards>() ;

    public Player(String name) {
        this.name = name;

    }

    // return score: how many round he wins
    public int getScore() {

        return score;
    }

    // when the player win a round his score increases by 1
    public void setScore() {
        this.score = score + 1;
    }

    // Add a card to player's hand
    public void addCard(Cards c) {
        hand.add(c);
    }

    // show the players  hand
    public List<Cards> showHand() {
        return hand;
    }

    // to check if player is protected
    public boolean playerProtected() {
        return playerProtected;

    }
    // to change the protected property
    public void setPlayerProtected (boolean safe){
        playerProtected = safe;
    }

    public String getName(){


      return name;

    }
 // return the list of discarded Cards
    public ArrayList<Cards> getCardsDiscarded () {
        return cardsDiscarded;
    }

    // asks what card to play
    // always set the protected state in false
    // force the player to play countess if he holds a king or prince with a countess
    // always called with the turnMove
    boolean t = true;
    public void playerPlay ( Game game , Cards card ) {

        setPlayerProtected(false);
        hand.add(card);
        if (countessCondition()) {
            playCountess(game, cI);
        } else {

            System.out.println(name + " your cards are:");
            System.out.println("first card:   " + hand.get(0).getNameOfCard());
            System.out.println("second card:  " + hand.get(1).getNameOfCard());
            System.out.println("Use \\1 to play first card or \\2 to play second card");

            while (t) {
                String s = game.scan.next();
                switch (s) {
                    case "\\1":
                        game.playFirstCard();
                        t = false;
                        System.out.println(t);
                        break;
                    case "\\2":
                        game.playSecondCard();
                        t = false;
                        break;

                }
            }
        }
    }

    // to check to countess condition
    // to set the countess index in the players hand
    public int cI ;
    private boolean countessCondition () {
        if (hand.get(0).getCardValue() == 7) {
            cI = 0;
            if (hand.get(1).getCardValue() == 6 || hand.get(1).getCardValue() == 5) {
                return true;
            }else {
                return false;
            }
        } else if (hand.get(1).getCardValue() == 7) {
            cI = 1;
            if (hand.get(0).getCardValue() == 6 || hand.get(0).getCardValue() == 5) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }}




   // c is the index of the card
    private void playCountess  (Game game, int c ){
        System.out.println(name + " your cards are");
        System.out.println("first card: " + hand.get(0).getNameOfCard());
        System.out.println("second card: " + hand.get(1).getNameOfCard());
        System.out.println("you have to choose the countess");
        game.playCard (hand.remove(c) , this);
    }
    //reset players for the next round
    public void resetPlayer (Cards card){
        hand = new ArrayList<Cards>();
        cardsDiscarded = new ArrayList<>();
        hand.add(card);
        setPlayerProtected(false);
    }
    // calculate the sum of values of the discarded cards

    public int sumDiscardedCards(){
        int result = 0  ;
        for (Cards c : cardsDiscarded ){
            result = result + c.getCardValue();
        }
        return result;
    }
}



