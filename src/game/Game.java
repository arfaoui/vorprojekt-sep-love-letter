package game;
import cards.*;
import game.Player;


import java.util.*;

public class Game {


    Scanner scan;
    public int playerNumber;
    // players names.
    String name1;
    String name2;
    String name3;
    String name4;

    Player newPlayer;

    // list of players
    public ArrayList<Player> players = new ArrayList<Player>();
    // list of active players is needed to define which players are still playing in the round
    public ArrayList<Player> activePlayers = new ArrayList<Player>();

    boolean gameIsRunning = false;
    // deck filled and shuffled
    private ArrayList<Cards> deck = new ArrayList<Cards>();
    //rounds needed to win depends on the players number.
    public ArrayList<Cards> discardedCards = new ArrayList<Cards>();
    public int roundNeeded;
    private Cards firstCardToflip;
    public ArrayList<Player> nextRoundActivePlayer = new ArrayList<Player>();


    public void game() {

        this.scan = new Scanner(System.in);
        int playerNumber = 0;
        System.out.println(" Welcome to LOVE LETTER, as a first step can you enter how many player. ");

        while (true) {

            String playerNumber1 = scan.next();

            try {
                playerNumber = Integer.parseInt(playerNumber1);
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong!!!!!!");

            }

            if (playerNumber == 2) {

                System.out.println("please every player write his name");
                name1 = scan.next();
                System.out.println("1st player :" + name1);
                newPlayer = new Player(name1);
                newPlayer.name = name1;
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name2 = scan.next();
                System.out.println("2nd player :" + name2);
                newPlayer = new Player(name2);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                this.playerNumber = 2;

                break;
            } else if (playerNumber == 3) {
                System.out.println("please every player write his name");

                name1 = scan.next();
                System.out.println("1st player :" + name1);
                newPlayer = new Player(name1);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name2 = scan.next();
                System.out.println("2nd player :" + name2);
                newPlayer = new Player(name2);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name3 = scan.next();
                System.out.println("3rd player :" + name3);
                newPlayer = new Player(name3);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                this.playerNumber = 3;

                break;
            } else if (playerNumber == 4) {
                System.out.println("please every player write his name");
                String name1 = scan.next();
                System.out.println("1st player :" + name1);
                newPlayer = new Player(name1);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name2 = scan.next();
                System.out.println("2nd player :" + name2);
                newPlayer = new Player(name2);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name3 = scan.next();
                System.out.println("3rd player :" + name3);
                newPlayer = new Player(name3);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                name4 = scan.next();
                System.out.println("4th player :" + name4);
                newPlayer = new Player(name4);
                players.add(newPlayer);
                activePlayers.add(newPlayer);
                this.playerNumber = 4;

                break;
            } else {
                System.out.println("number of player should be between 2 and 4");
                System.out.println("try again");

            }
            /** as a first part of the game user should enter the players number and their names before that nothing happens even commands don't exist
             The scan inputs are stored in variable which are declared  at the begging such as name1, playerNumber because they may be used in the later stages **/

        }

        System.out.println("Enter \\start to start the game");

        while (true) {
            String s = scan.next();
            if (s.equals("\\start")) {
                gameIsRunning = true;
                setRoundNeeded();
                System.out.println("Game starts");
                System.out.println("Enter the command \\help to output all commands and their functionality");
                System.out.println("there are " + playerNumber + " players: rounds needed to win the game are " + roundNeeded);
                System.out.println("to start the round enter \\startRound");
                System.out.println("------------------------------------------------------------------------");

                break;
            } else {
                System.out.println("Player should enter \\start to start the game");

            }
        }

        while (gameIsRunning) {
            String commands = scan.next();
            switch (commands) {


                case "\\help":
                    Commands helps = new Commands();
                    helps.help();
                    System.out.println();
                    break;

                case "\\playCard":
                    Commands playC = new Commands();
                    playC.playCard();
                    System.out.println(activePlayers.get(0).name);
                    break;

                /*case "\\showHand":
                    Commands show = new Commands();
                    show.showHand(newPlayer);
*/


                case "\\showScore":
                    Commands showS = new Commands();
                    showS.showScore(this, newPlayer);
                    break;
                case "\\showActivePlayerInTheRound":
                    Commands showAP = new Commands();
                    showAP.showActivePlayerInTheRound(this, newPlayer);

                    break;
                case "\\showAllPlayers":
                    Commands showA = new Commands();
                    showA.showAllPlayers(this, newPlayer);
                    break;
                case "\\exit":
                    gameIsRunning = false;
                case "\\startRound":
                    startRound();


            }
        }
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public List<Player> getActivePlayers(){
        return activePlayers;
    }

    public List<Player> getNextRoundActivePlayer() {
        return nextRoundActivePlayer;
    }
 // fill the deck and shuffle it
    public void deck() {

        deck = new ArrayList<Cards>(16);
        deck.add(new Princess());
        deck.add(new Countess());
        deck.add(new King());
        deck.add(new Prince());
        deck.add(new Prince());
        deck.add(new Handmaid());
        deck.add(new Handmaid());
        deck.add(new Baron());
        deck.add(new Baron());
        deck.add(new Priest());
        deck.add(new Priest());

        for (int i = 0; i < 5; i++)
            deck.add(new Guard());

        Collections.shuffle(deck);


    }

// to check if the deck id empty
    public boolean deckEmpty() {

        return deck.size() == 0;
    }

    // draw a card from the deck if the deck is not empty

    // drawnCard with the type Cards is the card drawn from the deck
    Cards drawnCard = null;

    public Cards drawCard() {

        if (deck.size() > 0) {
            drawnCard = deck.get(deck.size() - 1);
            deck.remove(deck.size() - 1);
        } else {
            System.out.println("deck is empty");
        }
        return drawnCard;
    }
    // inform all players that the player played a specific card
    // calls the action method in the class cards.Cards of the played card

    public void playCard(Cards playedCard, Player player) {
        System.out.println(player.name + " played the " + playedCard.getNameOfCard());
        playedCard.action(this, player);
        player.cardsDiscarded.add(playedCard);


    }
// to identify  the player with the best score
    private Player playerWithBestScore() {
        Player best = activePlayers.get(0);
        int s = activePlayers.get(0).getScore();
        for (int i = 1; i > playerNumber; i++) {
            if (players.get(i).getScore() >= s) {
                s = players.get(i).getScore();
                best = players.get(i);
            }
        }
        return best;

    }

    // start round
    public void startRound() {

        while (playerWithBestScore().getScore() < roundNeeded) {
            System.out.println("new round started ");
            // activePlayers = nextRoundActivePlayer;
            nextRoundActivePlayer = new ArrayList<Player>();
            // reactivate all eliminated players
            deck();
            // fill the deck
            firstCardToflip = drawCard();
            // flip first card

            for (int i = 1; i <= playerNumber; i++) {
                getPlayers().get(i - 1).resetPlayer(drawCard());
            }
            // in case of 2 players there are 3 discarded cards
            if (playerNumber == 2) {
                discardedCards.add(drawCard());
                discardedCards.add(drawCard());
                discardedCards.add(drawCard());
            }

            // after resetting the players,  refilling  the deck , filliping the first hidden card
            // turn moves to the first player
            turnMove(activePlayers.get(0));


        }

            System.out.println("The winner of the game is " + playerWithBestScore().name);
            System.out.println("------------------------------------------------------------------------");
            gameIsRunning = false;
        }


 // set the round tha a player needs to win the game
 // related to players number

    public void setRoundNeeded() {
        if (players.size() == 2) {
            this.roundNeeded = 7;
        } else if (playerNumber == 3) {
            this.roundNeeded = 5;
        } else {
            this.roundNeeded = 4;
        }
    }

    // starts a player move
    // if deck is empty starts another round and the winner of the round starts it
    public void turnMove(Player player) {
        if (getActivePlayers().size() > 1 && !deckEmpty()) {
            player.playerPlay(this, drawCard());
        } else if (activePlayers.size() == 1) {
            activePlayers.get(0).setScore();
            System.out.println("the winner of the round is " + activePlayers.get(0).name);
            activePlayers.get(0).setScore();
            System.out.println("new round starts ");
        } else {
            winners(activePlayers);
            winners(activePlayers).get(0).setScore();
            winners(activePlayers).get(0).setScore();
            System.out.println("the winner of the round is: " + activePlayers.get(0).getName());
            System.out.println("new round starts ");

        }

    }

    //determine winners of the round
    public List<Player> winners(ArrayList<Player> activePlayers) {
        ArrayList<Player> w = new ArrayList<>();
        w.add(activePlayers.get(0));
        for (int i = 1; i < activePlayers.size(); i++) {
            if (compare(w.get(0), activePlayers.get(i)) == 1) {
                return w;
            } else if (compare(w.get(0), activePlayers.get(i)) == -1) {
                w.remove(0);
                w.add(activePlayers.get(i));

            } else {
                w.add(activePlayers.get(0));

            }
        }
        return w;
    }
// play first card
    public void playFirstCard() {
        playCard(activePlayers.get(0).showHand().remove(0), activePlayers.get(0));
       // activePlayers.get(0).hand.remove(0);
    }
// play second card
    public void playSecondCard() {
        playCard(activePlayers.get(0).showHand().remove(1), activePlayers.get(0));
        //activePlayers.get(0).showHand().remove(1);
    }

    // compare to determine the winner of the round
    // compare between the value of the card in the hand
    // in a tie situation compare between the sum of the values of discarded cards
    public int compare(Player p1, Player p2) {
        if (p1.hand.get(0).getCardValue() > p2.hand.get(0).getCardValue()) {
            return 1;
        } else if (p1.hand.get(0).getCardValue() == p2.hand.get(0).getCardValue()) {
            if (p1.sumDiscardedCards() > p2.sumDiscardedCards()) {
                return 1;
            } else if (p1.sumDiscardedCards() == p2.sumDiscardedCards()) {
                return 0;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    // choose player needed in some cards effects
    public Player getPlayerByUserName(String playerName) {
        for (Player player :activePlayers){
            if (player.name.equals(playerName)){
                return player;
            }
        }
        return null ;

    }


// when the prince is played the chosen player have to discard a card with provoking the effect of card
// if the discarded card of the chosen player is a princess he is out

    public void noEffect(Cards playedcard, Player player) {
        discardedCards.add(0, playedcard);
        //we need to add it too to the list of cards discarded of the players to compare between them in case of tie
        player.cardsDiscarded.add(playedcard);
        if (playedcard.getCardValue() == 8) {
            System.out.println(player.name + " has played " + playedcard.getNameOfCard());
            playedcard.action(this, player);
        } else {
            System.out.println(player.name + " has discarded the " + playedcard.getNameOfCard() + " without effect");
            player.showHand().add(drawCard());

        }}
    // set game is running is used to end the game
    // in case player enter \exit
    // or a player won the game
        public boolean setGameIsRunning (){
            return false ;
        }
    }































        








