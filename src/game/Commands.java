package game;
import cards.Prince;
import cards.Princess;
import game.Player;
import game.Game;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;

public class Commands {
     public ArrayList<String> sH = new ArrayList<String>();

    // to output the score of all players
   public List showScore (Game game, Player player){
       System.out.println("show score");
       ArrayList <Integer> show = new ArrayList<Integer>(game.playerNumber);
                   for (int i = 0; i < game.playerNumber ; i++){
                       show.add(game.players.get(i).score);

                   }
                   System.out.println("number of players: "  + game.playerNumber);
                   System.out.println (show);
                   return show;
   }
   // To output all the players
    public List showAllPlayers (Game game, Player player) {

        ArrayList<String> show = new ArrayList<String>(game.playerNumber);
        for (int i = 0; i < game.playerNumber; i++) {
            show.add(game.players.get(i).name);

        }
        System.out.println(" number of player " + game.playerNumber);
        System.out.println(show);
        return show;
    }

    // to output the players hand
    public void showHand(Player newPlayer){
        System.out.println("Show hand of the player");

        sH.add(newPlayer.hand.get(0).getNameOfCard());
        if(newPlayer.hand.size()==2) {
            sH.add(newPlayer.hand.get(1).getNameOfCard());
        }
        System.out.println(sH);


   }

    public void playCard() {
        System.out.println("eu");
    }
    // to output all commands and their functionality
    public void help() {
        System.out.println("------------------------------------------------------------------------");
        System.out.println ("the program will ask for the number of player which should be between " +
                "2 and 4 and the name of every player. ");
        System.out.println ("To start the game the user should enter \\start (until this stage the code will just print start).");
        System.out.println ("\\help is a command to output all available commands with a brief explanation for the program.");
        System.out.println ("\\playCard is used to play a card (until this stage the programme will just print play card).");
        System.out.println ("\\showHand is used to show the hand of the player who called this command (until this stage the program" +
                " will just print show hand of the player).") ;
        System.out.println ("\\showScore is used to show the score of the players (until this stage the program will just print show scores).");
        System.out.println("\\showAllPlayers to show all the players");
        System.out.println("\\showActivePlayerInTheRound to show the active players in the game");
        System.out.println("\\startRound to start the round");
        System.out.println("\\1 to play the left card  (first card) ");
        System.out.println("\\2 to play the left card  (second card) ");
        System.out.println("to guess a card enter \\name of card ");
        System.out.println("to choose a player just enter the player's name");

        System.out.println("------------------------------------------------------------------------");


    }
    // to output all the active players
    public List showActivePlayerInTheRound(Game game, Player player) {


            ArrayList<String> showAP = new ArrayList<String>(game.activePlayers.size());
            for (int i = 0; i < game.getActivePlayers().size(); i++) {
                showAP.add(game.activePlayers.get(i).name);

            }
            System.out.println("in this round still " + game.activePlayers.size() + " active players" );
            System.out.println("The active players of the round are");
            System.out.println(showAP);
            return showAP;

    }





}
